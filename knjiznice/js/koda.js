
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */


function generirajPodatke(stPacienta){
    let data = partyData[stPacienta-1];
    createNewPatient(data, function(ehrIdOfCreated){
        if(ehrIdOfCreated != null){
            addNewComposition(ehrIdOfCreated, compositionData[stPacienta-1], function(){
                $('#preberiObstojeciEHR').append($("<option>").attr('value', ehrIdOfCreated).html(data.firstNames + " " + data.lastNames));
            });
        }
    });

}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

let bodyTemperature = {
    labels: [],
    temperatures: [],
    avg: null,
    min: null,
    max: null,
    graph: null
};

let bloodPressure = {
    labels: [],
    systolic: [],
    diastolic: [],
    avg: null,
    min: null,
    max: null,
    graph: null
}

let currentPatientSex = "female";
let currentPatientYearOfBirth = "1990";

function timeSort(a, b) {
    return new Date(a.time) - new Date(b.time);
}

function displayAndCalculate(bodyTempData, bloodPressureData, demographicData){
    
    //get year of birth from demographics data
    currentPatientYearOfBirth = new Date(demographicData.party.dateOfBirth).getFullYear();

    //sort data by time
    bodyTempData.sort(timeSort);
    bloodPressureData.sort(timeSort);

    //clear previous data
    bodyTemperature.labels = [];
    bodyTemperature.temperatures = [];
    bodyTemperature.min = 999;
    bodyTemperature.max = -1;

    let sum = 0;
    for(let i = 0; i<bodyTempData.length; i++){
        //create labels for bodyTemperature        
        bodyTemperature.labels.push(new Date(bodyTempData[i].time).toLocaleTimeString());
        
        //create data
        bodyTemperature.temperatures.push(bodyTempData[i].temperature);

        //calc min max and avg
        bodyTemperature.min = Math.min(bodyTempData[i].temperature, bodyTemperature.min);
        bodyTemperature.max = Math.max(bodyTempData[i].temperature, bodyTemperature.max);
        sum += bodyTempData[i].temperature;
    }
    bodyTemperature.avg = sum / bodyTempData.length;


    //clear previous data
    bloodPressure.labels = [];
    bloodPressure.systolic = [];
    bloodPressure.diastolic = [];
    bloodPressure.min = 999;
    bloodPressure.max = -1;

    sum = 0;
    for(let i = 0; i<bloodPressureData.length; i++){
        //create labels for bodyTemperature        
        bloodPressure.labels.push(new Date(bloodPressureData[i].time).toLocaleTimeString());
        
        //create data
        bloodPressure.systolic.push(bloodPressureData[i].systolic);
        bloodPressure.diastolic.push(bloodPressureData[i].diastolic);

        //calc min max and avg
        bloodPressure.min = Math.min(bloodPressureData[i].systolic, bloodPressureData.min);
        bloodPressure.max = Math.max(bloodPressureData[i].systolic, bloodPressure.max);
        sum += bloodPressureData[i].systolic;
    }
    bloodPressure.avg = sum / bloodPressureData.length;
    
    //update bodyTemperature graphs
    bodyTemperature.graph.data.labels = bodyTemperature.labels;
    bodyTemperature.graph.data.datasets[0].data = bodyTemperature.temperatures;
    bodyTemperature.graph.update();
    
    //update bloodpressure graphs
    bloodPressure.graph.data.labels = bloodPressure.labels;
    bloodPressure.graph.data.datasets[0].data = bloodPressure.systolic;
    bloodPressure.graph.data.datasets[1].data = bloodPressure.diastolic;
    bloodPressure.graph.update();
    
    //upadte avgs
    $('#body_temperature_avg').text(bodyTemperature.avg.toFixed(2));
    $('#blood_pressure_avg').text(bloodPressure.avg.toFixed(2));

    //expand all menus
    $('.collapsable').parents('.panel').find('.panel-body').slideDown();
    
    //show warnings and dangers
    $('#body_temperature_panel_status').attr('class', 'panel panel-default');
    if(bodyTemperature.max > 38.5 || bodyTemperature.min < 36.0){
        $('#body_temperature_panel_status').removeClass('panel-default');
        $('#body_temperature_panel_status').addClass('panel-warning');
        if(bodyTemperature.max > 40.0 || bodyTemperature.min < 35.5){
            $('#body_temperature_panel_status').addClass('panel-danger');
        }   
    }

    $('#blood_pressure_panel_status').attr('class', 'panel panel-default');
    if(bloodPressure.max > 130.0 || bloodPressure.min < 100){
        $('#blood_pressure_panel_status').removeClass('panel-default');
        $('#blood_pressure_panel_status').addClass('panel-warning');
        if(bloodPressure.max > 140.0 || bloodPressure.min < 90){
            $('#blood_pressure_panel_status').addClass('panel-danger');
        }   
    }

    if(demographicData.party.gender.toUpperCase() == "MALE"){
        $('#patient_sex').text("Moški");
        currentPatientSex = "male";
    }else{
        //female
        currentPatientSex = "female";
        $('#patient_sex').text("Ženska");
    }    

}


function preberiEHRodBolnika(){
    let ehrId = $('#preberiEHRid').val();
    let selectedDate = $('#date_input').get(0).valueAsDate;
    //1996-02-02T00:00:00.000Z
    let params = {
        "from" : $.format.date(selectedDate, "yyyy-MM-ddT00:00:00.000Z"),
        "to" : $.format.date(selectedDate, "yyyy-MM-ddT23:59:00.000Z")
    }
    console.log("Params = " + JSON.stringify(params));

    $.ajax(
        {
            headers: {
                "Ehr-Session" : getSessionId()
            },
            url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
            type: "GET",
            success: function(demoData){
                console.log(JSON.stringify(demoData));

                $.ajax(
                    {
                        headers: {
                            "Ehr-Session" : getSessionId()
                        },
                        url: baseUrl + "/view/" + ehrId + "/body_temperature?" + $.param(params),
                        type: "GET",
                        success: function(bodyTempData){
                            $.ajax(
                                {
                                    headers: {
                                        "Ehr-Session" : getSessionId()
                                    },
                                    url: baseUrl + "/view/" + ehrId + "/blood_pressure?" + $.param(params),
                                    type: "GET",
                                    success: function(bloodPressureData){
                                        displayAndCalculate(bodyTempData, bloodPressureData, demoData);
                                    },
                        
                                }
                            );
                        },
            
                    }
                );

            }
        }
    );   
    
}


function staticDataGeneration() {

    //clear current drop down
    $('#preberiObstojeciEHR').html("");

  	for(let i = 1; i <= 3; i++){
  		generirajPodatke(i);
  	}

}

$(document).on('click', '.collapsable', function(e){
    var $this = $(this);
	if(!$this.hasClass('panel-collapsed')) {
		$this.parents('.panel').find('.panel-body').slideUp();
		$this.addClass('panel-collapsed');
	} else {
		$this.parents('.panel').find('.panel-body').slideDown();
		$this.removeClass('panel-collapsed');
	}
});

$(document).ready(function() {
    $('.collapsable').parents('.panel').find('.panel-body').hide();
    $('.collapsable').addClass('panel-collapsed');

    //add ehrField auto insert
    $('#preberiObstojeciEHR').change(function(){
       $('#preberiEHRid').val($(this).val());
    });



    //create charts statically and reference them in obj
    
    let canvasBody = $("<canvas>");
    $("#body_temperature_chart_div").html("");
    $("#body_temperature_chart_div").append(canvasBody);
    bodyTemperature.graph = new Chart(canvasBody[0].getContext('2d'), {
        type: 'line',
        data: {
            labels: [],
            datasets: [{ 
                data: [],
                label: "Telesna temperatura",
                borderColor: "#3e95cd",
                fill: false
            }]
        },
          options: {
              scales: {
                yAxes: [{
                    display: true,
                    ticks: {
                        suggestedMin: 32,
                        suggestedMax: 44
                    }
                }]
            }
        }
    });
    
    let canvasBlood = $("<canvas>");
    $("#blood_pressure_chart_div").html("");
    $("#blood_pressure_chart_div").append(canvasBlood);
    bloodPressure.graph = new Chart( canvasBlood[0].getContext('2d'), {
        type: 'line',
        data : {
            labels: [],
        datasets: [{ 
            data: [],
            label: "Sistolični pritisk",
            borderColor: "#3e95cd",
            fill: false
          }, { 
            data: [],
            label: "Diastolični pritisk",
            borderColor: "#8e5ea2",
            fill: false
          }]
        },
          options: {
            scales: {
              yAxes: [{
                  display: true,
                  ticks: {
                      suggestedMin: 60,
                      suggestedMax: 200
                  }
              }]
          }
        }
    });


    //populate dataset
    let symptomsData = JSON.parse(SYMPTOMS_DATA_JSON);
    for(let i = 0; i<symptomsData.length; i++){
        let option = $('<option>');
        option.val(symptomsData[i].ID);
        option.text(symptomsData[i].Name);
        $('#symptoms_input').append(option);
    }

});

let apiMedicUsername = "mp4511@student.uni-lj.si";
let apiMedicPassword = "n2N8Mtx4B5SiTb76K";

var apiMedicLoginUrl = "https://sandbox-authservice.priaid.ch/login";
let apiMedicBaseUrl = 'https://sandbox-healthservice.priaid.ch';

function getDiagnosis() {
        
    //read all the selected symptoms
    let symptoms = $('#extra_symptoms li')
    let sIds = [];
    for(let i = 0; i<symptoms.length; i++){
        sIds.push(parseInt(symptoms[i].getAttribute('id')));
    }

    console.log("sIds: " + JSON.stringify(sIds));

    var computedHash = CryptoJS.HmacMD5(apiMedicLoginUrl, apiMedicPassword);
    var computedHashString = computedHash.toString(CryptoJS.enc.Base64);     
    console.log("HashString = " + computedHashString);

    //login to apimedic
    $.ajax({
        type: "POST",
        url: apiMedicLoginUrl,
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + apiMedicUsername + ':' + computedHashString);
        },
        success: function(tokenData){
            let apiMedicToken = tokenData.Token;
            $.ajax({
                type: "GET",
                url: apiMedicBaseUrl + '/diagnosis?language=en-gb&symptoms=' + JSON.stringify(sIds) + '&gender=' + currentPatientSex + '&year_of_birth=' + currentPatientYearOfBirth + '&token=' + apiMedicToken,
                success: function(data){
                    let list = $('#apimedic_diagnosis');
                    //clear previous diagnosis
                    list.html("");
                
                    if(data.length > 0){
                        for(let i = 0; i<5 && i<data.length; i++){
                            console.log(data);
                            let element = $('<li>');
                            element.text(data[i].Issue.Name + " (" + data[i].Issue.Accuracy.toFixed(1) + "%)");
                            list.append(element);
                        }
                    } else{
                        list.append($('<li>Ni rezultatov za izbrane simptome</li>'));
                    }
        
                }
            });
        
        }
    });

}

function addToListOfSymptoms(){
    let listItem = $("<li>");
    let symIn = $('#symptoms_input');
    listItem.attr('id', symIn.val());
    listItem.text($("#symptoms_input option:selected").text());
    let listRemove = $('<button type="button" class="btn btn-primary btn-xs" onclick="removeListItem('+ symIn.val() +')">');
    listRemove.text('-');
    listItem.append(listRemove);
    $('#extra_symptoms').append(listItem);
}

function removeListItem(id){
    $('#extra_symptoms li[id=' + id + ']').remove();
}

var queryParams = {
    "ehrId": null,
    templateId: 'Vital Signs',
    format: 'FLAT',
    committer: 'Belinda Nurse'
};
function addNewComposition(ehrId, composition, callback){
    let params = JSON.parse(JSON.stringify(queryParams));
    params.ehrId = ehrId;
    
    var sessionId = getSessionId();

    $.ajaxSetup({
        headers: {"Ehr-Session": sessionId }
    });

    $.ajax({
        url: baseUrl + "/composition?" + $.param(params),
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(composition),
        success: function (res) {
            callback();
        }
    });
}


function createNewPatient(partyData, callback){
    
    var sessionId = getSessionId();

    $.ajaxSetup({
        headers: {"Ehr-Session": sessionId }
    });
    
	$.ajax({
	    url: baseUrl + "/ehr",
	    type: 'POST',
	    success: function (data) {
	        var ehrId = data.ehrId;
            partyData.partyAdditionalInfo[0].value = ehrId;
            console.log("PartyData = " + JSON.stringify(partyData));
	        
	        $.ajax({
	            url: baseUrl + "/demographics/party",
	            type: 'POST',
	            contentType: 'application/json',
	            data: JSON.stringify(partyData),
	            success: function (party) {
	                if (party.action == 'CREATE') {
	                    callback(ehrId);
	                }else{
	                	callback(null);
	                }
	            }
	        });
	    }
	});

}




let partyData = [
	{
	    firstNames: "Matic",
        lastNames: "Brzostrel",
        gender: "MALE",
	    dateOfBirth: "1996-02-02T00:00:00.000Z",
		partyAdditionalInfo: [
			{
				key: "ehrId",
	            value: null
	        }
	    ]
	 },
	 {
	    firstNames: "Alen",
        lastNames: "Opehavnik",
        gender: "MALE",
	    dateOfBirth: "1976-03-02T00:00:00.000Z",
		partyAdditionalInfo: [
			{
				key: "ehrId",
	            value: null
	        }
	    ]
	 },
	 {
	    firstNames: "Karin",
        lastNames: "Srednjeveska",
        gender: "FEMALE",
	    dateOfBirth: "1942-04-04T00:00:00.000Z",
		partyAdditionalInfo: [
			{
				key: "ehrId",
	            value: null
	        }
	    ]
	 },
];




var compositionData = [
	//in shape
	{
    "ctx/time": "2018-05-25T13:10Z",
    "ctx/language": "en",
    "ctx/territory": "SI",
    "vital_signs/body_temperature/any_event:0/temperature|magnitude": 37.1,
    "vital_signs/body_temperature/any_event:0/temperature|unit": "°C",
    "vital_signs/body_temperature/any_event:0/time": "2018-05-25T08:00:00.000Z",

    "vital_signs/body_temperature/any_event:1/temperature|magnitude": 37.2,
    "vital_signs/body_temperature/any_event:1/temperature|unit": "°C",
    "vital_signs/body_temperature/any_event:1/time": "2018-05-25T10:00:00.000Z",
    
    "vital_signs/body_temperature/any_event:2/temperature|magnitude": 37.3,
    "vital_signs/body_temperature/any_event:2/temperature|unit": "°C",
    "vital_signs/body_temperature/any_event:2/time": "2018-05-25T12:00:00.000Z",
    
    "vital_signs/body_temperature/any_event:3/temperature|magnitude": 37.4,
    "vital_signs/body_temperature/any_event:3/temperature|unit": "°C",
    "vital_signs/body_temperature/any_event:3/time": "2018-05-25T14:00:00.000Z",

    "vital_signs/body_temperature/any_event:4/temperature|magnitude": 37.4,
    "vital_signs/body_temperature/any_event:4/temperature|unit": "°C",
    "vital_signs/body_temperature/any_event:4/time": "2018-05-25T16:00:00.000Z",
    
    "vital_signs/body_temperature/any_event:5/temperature|magnitude": 37.4,
    "vital_signs/body_temperature/any_event:5/temperature|unit": "°C",
    "vital_signs/body_temperature/any_event:5/time": "2018-05-25T18:00:00.000Z",
    
    "vital_signs/body_temperature/any_event:6/temperature|magnitude": 37.1,
    "vital_signs/body_temperature/any_event:6/temperature|unit": "°C",
    "vital_signs/body_temperature/any_event:6/time": "2018-05-25T20:00:00.000Z",
    
    "vital_signs/blood_pressure/any_event:0/systolic": 122,
    "vital_signs/blood_pressure/any_event:0/diastolic": 92,    
    "vital_signs/blood_pressure/any_event:0/time": "2018-05-25T08:00:00.000Z",

    "vital_signs/blood_pressure/any_event:1/systolic": 121,
    "vital_signs/blood_pressure/any_event:1/diastolic": 93,
    "vital_signs/blood_pressure/any_event:1/time": "2018-05-25T10:00:00.000Z",

    "vital_signs/blood_pressure/any_event:2/systolic": 122,
    "vital_signs/blood_pressure/any_event:2/diastolic": 91,    
    "vital_signs/blood_pressure/any_event:2/time": "2018-05-25T12:00:00.000Z",

    "vital_signs/blood_pressure/any_event:3/systolic": 120,
    "vital_signs/blood_pressure/any_event:3/diastolic": 91,    
    "vital_signs/blood_pressure/any_event:3/time": "2018-05-25T14:00:00.000Z",

    "vital_signs/blood_pressure/any_event:4/systolic": 121,
    "vital_signs/blood_pressure/any_event:4/diastolic": 90,    
    "vital_signs/blood_pressure/any_event:4/time": "2018-05-25T16:00:00.000Z",

    "vital_signs/blood_pressure/any_event:5/systolic": 124,
    "vital_signs/blood_pressure/any_event:5/diastolic": 93,    
    "vital_signs/blood_pressure/any_event:5/time": "2018-05-25T18:00:00.000Z",

    "vital_signs/blood_pressure/any_event:6/systolic": 122,
    "vital_signs/blood_pressure/any_event:6/diastolic": 91,    
    "vital_signs/blood_pressure/any_event:6/time": "2018-05-25T20:00:00.000Z",



	},

	//mediocre healty
	{
    "ctx/time": "2018-05-25T13:10Z",
    "ctx/language": "en",
    "ctx/territory": "SI",
    "vital_signs/body_temperature/any_event:0/temperature|magnitude": 37.7,
    "vital_signs/body_temperature/any_event:0/temperature|unit": "°C",
    "vital_signs/body_temperature/any_event:0/time": "2018-05-25T08:00:00.000Z",

    "vital_signs/body_temperature/any_event:1/temperature|magnitude": 38.2,
    "vital_signs/body_temperature/any_event:1/temperature|unit": "°C",
    "vital_signs/body_temperature/any_event:1/time": "2018-05-25T10:00:00.000Z",
    
    "vital_signs/body_temperature/any_event:2/temperature|magnitude": 38.7,
    "vital_signs/body_temperature/any_event:2/temperature|unit": "°C",
    "vital_signs/body_temperature/any_event:2/time": "2018-05-25T12:00:00.000Z",
    
    "vital_signs/body_temperature/any_event:3/temperature|magnitude": 39.2,
    "vital_signs/body_temperature/any_event:3/temperature|unit": "°C",
    "vital_signs/body_temperature/any_event:3/time": "2018-05-25T14:00:00.000Z",

    "vital_signs/body_temperature/any_event:4/temperature|magnitude": 39.8,
    "vital_signs/body_temperature/any_event:4/temperature|unit": "°C",
    "vital_signs/body_temperature/any_event:4/time": "2018-05-25T16:00:00.000Z",
    
    "vital_signs/body_temperature/any_event:5/temperature|magnitude": 40.4,
    "vital_signs/body_temperature/any_event:5/temperature|unit": "°C",
    "vital_signs/body_temperature/any_event:5/time": "2018-05-25T18:00:00.000Z",
    
    "vital_signs/body_temperature/any_event:6/temperature|magnitude": 40.9,
    "vital_signs/body_temperature/any_event:6/temperature|unit": "°C",
    "vital_signs/body_temperature/any_event:6/time": "2018-05-25T20:00:00.000Z",
    
    "vital_signs/blood_pressure/any_event:0/systolic": 122,
    "vital_signs/blood_pressure/any_event:0/diastolic": 92,    
    "vital_signs/blood_pressure/any_event:0/time": "2018-05-25T08:00:00.000Z",

    "vital_signs/blood_pressure/any_event:1/systolic": 121,
    "vital_signs/blood_pressure/any_event:1/diastolic": 93,
    "vital_signs/blood_pressure/any_event:1/time": "2018-05-25T10:00:00.000Z",

    "vital_signs/blood_pressure/any_event:2/systolic": 122,
    "vital_signs/blood_pressure/any_event:2/diastolic": 91,    
    "vital_signs/blood_pressure/any_event:2/time": "2018-05-25T12:00:00.000Z",

    "vital_signs/blood_pressure/any_event:3/systolic": 120,
    "vital_signs/blood_pressure/any_event:3/diastolic": 91,    
    "vital_signs/blood_pressure/any_event:3/time": "2018-05-25T14:00:00.000Z",

    "vital_signs/blood_pressure/any_event:4/systolic": 121,
    "vital_signs/blood_pressure/any_event:4/diastolic": 90,    
    "vital_signs/blood_pressure/any_event:4/time": "2018-05-25T16:00:00.000Z",

    "vital_signs/blood_pressure/any_event:5/systolic": 124,
    "vital_signs/blood_pressure/any_event:5/diastolic": 93,    
    "vital_signs/blood_pressure/any_event:5/time": "2018-05-25T18:00:00.000Z",

    "vital_signs/blood_pressure/any_event:6/systolic": 122,
    "vital_signs/blood_pressure/any_event:6/diastolic": 91,    
    "vital_signs/blood_pressure/any_event:6/time": "2018-05-25T20:00:00.000Z",

	},

	//not healty
	{
    "ctx/time": "2018-05-25T13:10Z",
    "ctx/language": "en",
    "ctx/territory": "SI",
    "vital_signs/body_temperature/any_event:0/temperature|magnitude": 37.7,
    "vital_signs/body_temperature/any_event:0/temperature|unit": "°C",
    "vital_signs/body_temperature/any_event:0/time": "2018-05-25T08:00:00.000Z",

    "vital_signs/body_temperature/any_event:1/temperature|magnitude": 37.2,
    "vital_signs/body_temperature/any_event:1/temperature|unit": "°C",
    "vital_signs/body_temperature/any_event:1/time": "2018-05-25T10:00:00.000Z",
    
    "vital_signs/body_temperature/any_event:2/temperature|magnitude": 36.8,
    "vital_signs/body_temperature/any_event:2/temperature|unit": "°C",
    "vital_signs/body_temperature/any_event:2/time": "2018-05-25T12:00:00.000Z",
    
    "vital_signs/body_temperature/any_event:3/temperature|magnitude": 36.2,
    "vital_signs/body_temperature/any_event:3/temperature|unit": "°C",
    "vital_signs/body_temperature/any_event:3/time": "2018-05-25T14:00:00.000Z",

    "vital_signs/body_temperature/any_event:4/temperature|magnitude": 35.8,
    "vital_signs/body_temperature/any_event:4/temperature|unit": "°C",
    "vital_signs/body_temperature/any_event:4/time": "2018-05-25T16:00:00.000Z",
    
    "vital_signs/body_temperature/any_event:5/temperature|magnitude": 35.5,
    "vital_signs/body_temperature/any_event:5/temperature|unit": "°C",
    "vital_signs/body_temperature/any_event:5/time": "2018-05-25T18:00:00.000Z",
    
    "vital_signs/body_temperature/any_event:6/temperature|magnitude": 35.5,
    "vital_signs/body_temperature/any_event:6/temperature|unit": "°C",
    "vital_signs/body_temperature/any_event:6/time": "2018-05-25T20:00:00.000Z",
    
    "vital_signs/blood_pressure/any_event:0/systolic": 140,
    "vital_signs/blood_pressure/any_event:0/diastolic": 95,    
    "vital_signs/blood_pressure/any_event:0/time": "2018-05-25T08:00:00.000Z",

    "vital_signs/blood_pressure/any_event:1/systolic": 145,
    "vital_signs/blood_pressure/any_event:1/diastolic": 98,
    "vital_signs/blood_pressure/any_event:1/time": "2018-05-25T10:00:00.000Z",

    "vital_signs/blood_pressure/any_event:2/systolic": 153,
    "vital_signs/blood_pressure/any_event:2/diastolic": 93,    
    "vital_signs/blood_pressure/any_event:2/time": "2018-05-25T12:00:00.000Z",

    "vital_signs/blood_pressure/any_event:3/systolic": 158,
    "vital_signs/blood_pressure/any_event:3/diastolic": 91,    
    "vital_signs/blood_pressure/any_event:3/time": "2018-05-25T14:00:00.000Z",

    "vital_signs/blood_pressure/any_event:4/systolic": 162,
    "vital_signs/blood_pressure/any_event:4/diastolic": 93,    
    "vital_signs/blood_pressure/any_event:4/time": "2018-05-25T16:00:00.000Z",

    "vital_signs/blood_pressure/any_event:5/systolic": 161,
    "vital_signs/blood_pressure/any_event:5/diastolic": 92,    
    "vital_signs/blood_pressure/any_event:5/time": "2018-05-25T18:00:00.000Z",

    "vital_signs/blood_pressure/any_event:6/systolic": 162,
    "vital_signs/blood_pressure/any_event:6/diastolic": 94,    
    "vital_signs/blood_pressure/any_event:6/time": "2018-05-25T20:00:00.000Z",

	}

];
